package com.masterpik.hub;

import com.comphenix.packetwrapper.WrapperPlayServerEntityHeadRotation;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.masterpik.database.db.Connect;
import com.masterpik.hub.commands.GeneralCmd;
import com.masterpik.hub.commands.PlayersCmd;
import com.masterpik.hub.confy.AnimalsConfy;
import com.masterpik.hub.confy.GeneralConfy;
import com.masterpik.hub.confy.PlayerInventoryConfy;
import com.masterpik.messages.spigot.Api;
import com.sainttx.holograms.internal.HologramImpl;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;

public class Main extends JavaPlugin implements Runnable {

    public static ArrayList<Item> menuItems = new ArrayList<Item>();
    public static HashMap<String, ArrayList<Inventory>> inventoriesMap = new HashMap<String, ArrayList<Inventory>>();
    public static Plugin plugin = Bukkit.getPluginManager().getPlugin("Hub");
    public static Plugin holoPlugin = Bukkit.getPluginManager().getPlugin("HolographicDisplays");
    public static BungeeUtils bungee;
    public static ArrayList<HologramImpl> holos = new ArrayList<HologramImpl>();
    public static Inventory PlayerInventory;

    public static Inventory jumpsInventory;

    public static Connection connection;

    public static HashMap<String, CountServ> PlayersCS;

    public static Player fakePlayer;

    public static ProtocolManager protocolManager;

    public static String masterpikPrefix;
    public static String playerJoinMsg;
    public static String playerQuitMsg;
    public static String winJumps;



    public static ArrayList<Player> notBoat;

    @Override
    public void onEnable() {

        plugin = Bukkit.getPluginManager().getPlugin("Hub");
        holoPlugin = Bukkit.getPluginManager().getPlugin("Holograms");

        PlayersCS = new HashMap<>();

        //getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        Main.bungee = new BungeeUtils(this, "gamesCo");
        Bukkit.getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new BungeeWork(this));

        Listener listen = new Events();
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(listen, this);

        getCommand("spawn").setExecutor(new PlayersCmd());
        getCommand("hp").setExecutor(new GeneralCmd());

        AnimalsConfy.animalsInit();
        PlayerInventoryConfy.generalInit();
        GeneralConfy.generalInit();

        Main.PlayerInventory = PlayerInventoryConfy.getInventory();

        Bukkit.getScheduler().scheduleSyncDelayedTask(this, this, 10L);

        connection = Connect.getConnection();

        /*FakePlayer fakeP = new FakePlayer("hub");
        fakeP.spawn();

        fakePlayer = (Player) CraftPlayer.getEntity((CraftServer) Bukkit.getServer(), fakeP.getEp());*/

        /*protocolManager = ProtocolLibrary.getProtocolManager();

        protocolManager.addPacketListener(
                new PacketAdapter(this, ListenerPriority.LOWEST, PacketType.Play.Server.ENTITY_HEAD_ROTATION) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        event.getPlayer().sendMessage("Entity Head Rotation Packet (source : "+event.getSource().toString()+")");
                    }
                });

        protocolManager.addPacketListener(
                new PacketAdapter(this, ListenerPriority.LOWEST, PacketType.Play.Server.ENTITY_LOOK) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        if (event.getPacketType() == PacketType.Play.Server.ENTITY_LOOK) {
                        }
                    }
                });*/

        masterpikPrefix = Api.getString("general.masterpik.main.logoTextChat");
        playerJoinMsg = Api.getString("general.masterpik.detection.playerJoin");
        playerQuitMsg = Api.getString("general.masterpik.detection.playerQuit");

        winJumps = Api.getString("hub.detection.jumpsFinish");


        notBoat = new ArrayList<>();

    }

    @Override
    public void onDisable() {
        AnimalsManagement.killAll();

        Bukkit.getPluginManager().enablePlugin(Main.holoPlugin);
        AnimalsManagement.killHolos();
        Bukkit.getPluginManager().disablePlugin(Main.holoPlugin);
        AnimalsManagement.killAll();
    }

    @Override
    public void run() {
        Utils.InitHub(GeneralConfy.getHubWorld());
        AnimalsManagement.spawnAll();

        jumpsInventory = Main.inventoriesMap.get("hubgame").get(1);


        CountServManagement.CountServInit();
        CountServManagement.startTimer(5);


    }

}
