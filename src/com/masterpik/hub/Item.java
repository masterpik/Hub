package com.masterpik.hub;

import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class Item {

    String animal;
    ItemStack item;
    int type;
    ArrayList<String> link;
    ArrayList<String> countLink;
    boolean isCount;

    public Item(String panimal, ItemStack pitem, int ptype, ArrayList<String> pargs, boolean PisCount, ArrayList<String> pCountLink) {
        setAnimal(panimal);
        setItemStack(pitem);
        setType(ptype);
        setLink(pargs);

        this.isCount = PisCount;

        this.countLink = pCountLink;
    }


    public void setAnimal(String panimal) {
        animal = panimal;
    }
    public String getAnimal() {
        return animal;
    }

    public void setItemStack(ItemStack pitem) {
        item = pitem;
    }
    public ItemStack getItemStack() {
        return item;
    }

    public void setType(int ptype) {
        type = ptype;
    }
    public int getType() {
        return type;
    }

    public void setLink(ArrayList<String> plink) {
        link = plink;
    }
    public ArrayList<String> getLink() {
        return link;
    }


    public boolean isCount() {
        return isCount;
    }
    public void setIsCount(boolean PisCount) {
        this.isCount = PisCount;
    }

    public ArrayList<String> getCountLink() {
        return countLink;
    }
    public void setCountLink(ArrayList<String> PcountLink) {
        this.countLink = PcountLink;
    }
}
