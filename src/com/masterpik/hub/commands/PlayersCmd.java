package com.masterpik.hub.commands;

import com.masterpik.hub.confy.GeneralConfy;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayersCmd implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (command.getName().equalsIgnoreCase("spawn")) {
                player.teleport(GeneralConfy.getSpawnHubLocation());
                return true;
            }
        }
        else {
            sender.sendMessage("Vous devez etre un joueur pour executer cette commande");
        }
        return false;
    }
}
