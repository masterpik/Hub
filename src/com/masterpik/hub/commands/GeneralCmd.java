package com.masterpik.hub.commands;

import com.masterpik.hub.AnimalsManagement;
import com.masterpik.hub.Main;
import com.masterpik.hub.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GeneralCmd implements CommandExecutor{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender.isOp()) {
            if (command.getName().equalsIgnoreCase("hp")) {
                int nbArgs = args.length;

                if (nbArgs == 0) {
                    return false;
                }
                else if (nbArgs == 1) {
                    if (args[0].equalsIgnoreCase("reload")) {

                        Utils.reloadPlugin();

                        return true;
                    }
                    else if (args[0].equalsIgnoreCase("getMenu")) {

                        if (sender instanceof Player) {
                          ((Player) sender).getInventory().setContents(Main.PlayerInventory.getContents());
                        }

                        return true;
                    }
                }

            }
        }

        return false;
    }
}
