package com.masterpik.hub;

import com.comphenix.packetwrapper.WrapperPlayServerEntityLook;
import com.masterpik.api.PacketWrapper.WrapperPlayServerEntityHeadRotation;
import com.masterpik.api.util.UtilArrayList;
import com.masterpik.api.util.UtilString;
import com.masterpik.api.util.UtilVector;
import com.masterpik.database.db.Connect;
import com.masterpik.hub.confy.GeneralConfy;
import com.masterpik.hub.confy.PlayerInventoryConfy;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_9_R1.TrigMath;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static String ArrayToString(ArrayList<String> list) {
        /*String string = "";

        int bucle = 0;

        while (bucle < list.size()) {

            if (bucle == 0) {
                string = ""+bucle+":("+list.get(bucle)+")";
            }
            else {
                string = ""+string+""+bucle+":("+list.get(bucle)+")";
            }

            bucle++;
        }


        return string;*/
        return UtilArrayList.ArrayListToCryptedString(list);
    }

    public static ArrayList<String> StringToArray(String string) {
        /*ArrayList<String> list = new ArrayList<String>();

        int bucle1 = 0;
        int bucle2 = 0;
        int count = 0;
        int bcl = 0;
        boolean okay1 = false;
        boolean finish = false;
        int calculsavant = 0;

        bcl = 0;
        while (!finish) {

            //Bukkit.getLogger().info("buclefinish, bcl : "+bcl);

            if (string.length() > bcl) {

                //Bukkit.getLogger().info("firstCond : "+string.charAt(bcl));
                //Bukkit.getLogger().info("secondCon : "+Integer.toString(count).charAt(0));

                if (string.charAt(bcl) == Integer.toString(count).charAt(0)
                        && string.charAt((bcl + 1)) == ':'
                        && string.charAt((bcl + 2)) == '(') {

                    bucle1 = bcl + 3;

                    okay1 = false;
                    while (!okay1) {

                        //Bukkit.getLogger().info("bucle1 while : "+bucle1);

                        if (string.charAt(bucle1) == ')') {
                            String fin = "";

                            calculsavant = 0;
                            calculsavant = (bucle1 - ((bcl + 3)));

                            bucle1 = bucle1 - calculsavant;

                            bucle2 = 0;

                            while (bucle2 < calculsavant) {

                                //Bukkit.getLogger().info("resultOfCalculSavant : "+calculsavant);

                                //Bukkit.getLogger().info("CharAt : "+(bucle1+bucle2));
                                fin = "" + fin + "" + string.charAt(bucle1 + bucle2);

                                bucle2++;
                            }
                            //Bukkit.getLogger().info("bucle2-1 : "+(bucle2));

                            list.add(fin);
                            //Bukkit.getLogger().info("La final : "+ fin);
                            okay1 = true;
                        }

                        bucle1++;
                    }
                    //Bukkit.getLogger().info("bucle1-1 : "+(bucle1-1));

                }
            }
            else {
                finish = true;
            }
            bcl = bcl+4+(bucle2);
            count++;
        }

        return list;*/
        return UtilString.CryptedStringToArrayList(string);
    }

    public static void RealClear(Player player) {
        player.getInventory().clear();
        player.getInventory().setHelmet(new ItemStack(Material.AIR));
        player.getInventory().setChestplate(new ItemStack(Material.AIR));
        player.getInventory().setLeggings(new ItemStack(Material.AIR));
        player.getInventory().setBoots(new ItemStack(Material.AIR));

        player.setLevel(0);

        for(PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }

    }

    public static void InitHub(World world) {
        String trou = "true";
        String folse = "false";

        world.setFullTime(6000L);

        world.setGameRuleValue("commandBlockOutput", folse);
        world.setGameRuleValue("doDaylightCycle", folse);
        world.setGameRuleValue("doEntityDrops", folse);
        world.setGameRuleValue("doFireTick", folse);
        world.setGameRuleValue("doMobLoot", folse);
        world.setGameRuleValue("doMobSpawning", folse);
        world.setGameRuleValue("doTileDrops", trou);
        world.setGameRuleValue("keepInventory", trou);
        world.setGameRuleValue("logAdminCommands", trou);
        world.setGameRuleValue("mobGriefing", folse);
        world.setGameRuleValue("naturalRegeneration", trou);
        world.setGameRuleValue("randomTickSpeed", "3");
        world.setGameRuleValue("reducedDebugInfo", trou);
        world.setGameRuleValue("sendCommandFeedback", folse);
        world.setGameRuleValue("showDeathMessages", folse);

        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset animals false "+world.getName());
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset monsters false "+world.getName());

        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset pvp false "+world.getName());

        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset diff 2 "+world.getName());
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
                "mvmset weather false "+world.getName());
    }

    public static void seeAll(Player player) {

        for (Player pl : Bukkit.getOnlinePlayers()) {
            pl.canSee(player);
            player.canSee(pl);

            if (pl.getWorld().equals(GeneralConfy.getHubWorld())) {
                for (Entity entity : GeneralConfy.getHubWorld().getEntities()) {
                    if (entity.hasMetadata("name")) {
                        float beta = Utils.getBeta(entity, pl);
                        float beta2 = Utils.getBeta2(entity, pl);

                        WrapperPlayServerEntityHeadRotation packet1 = new WrapperPlayServerEntityHeadRotation();

                        packet1.setEntityId(entity.getEntityId());
                        packet1.setHeadYaw(beta);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                packet1.sendPacket(pl);
                            }
                        }, 1L);


                        WrapperPlayServerEntityLook packet = new WrapperPlayServerEntityLook();

                        packet.setEntityID(entity.getEntityId());
                        packet.setYaw(beta);
                        packet.setPitch(beta2);
                        packet.setOnGround(true);

                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                packet.sendPacket(pl);
                            }
                        }, 1L);
                    }
                }
            }
        }

        /*int bucle = 0;
        List<Player> players = new ArrayList<Player>();
        players.addAll(Bukkit.getOnlinePlayers());
        while (bucle < players.size()) {

            players.get(bucle).canSee(player);
            player.canSee(players.get(bucle));
            bucle++;
        }*/
    }

    public static void reloadPlugin() {
        AnimalsManagement.killAll();
        AnimalsManagement.killHolos();

        Main.holos.clear();
        Main.menuItems.clear();
        Main.inventoriesMap.clear();

        Main.PlayerInventory = PlayerInventoryConfy.getInventory();

        AnimalsManagement.spawnAll();

        Main.connection = Connect.getConnection();
    }

    public static float getBeta(Entity entity, Player player) {
        /*Location A = entity.getLocation();
        double XA = A.getX();
        double YA = A.getY();
        YA = YA + 2;
        double ZA = A.getZ();

        Location B = player.getLocation();
        double XB = B.getX();
        double YB = B.getY();
        YB = YB + 1;
        double ZB = B.getZ();


        double alpha = Math.toDegrees(Math.atan((ZB - ZA) / (XB - XA)));


        float beta = A.getYaw();

        if (ZB > ZA && XB < XA) {
            beta = (float) (90 + alpha);
        } else if (ZB < ZA && XB < XA) {
            beta = (float) (90 + alpha);
        } else if (ZB < ZA && XB > XA) {
            beta = (float) (-90 + alpha);
        } else if (ZB > ZA && XB > XA) {
            beta = (float) (-90 + alpha);
        } else if (ZB == ZA && XB == XA) {
            beta = beta;
        } else if (ZB == ZA) {
            if (XB > XA) {
                beta = -90;
            } else if (XB < XA) {
                beta = 90;
            }
        } else if (XB == XA) {
            if (ZB > ZA) {
                beta = 0;
            } else if (ZB < ZA) {
                beta = 180;
            }
        }

        return beta;*/
        return UtilVector.getYawLocations(entity.getLocation(), player.getLocation());
        //return getLookAtYaw(entity.getLocation(), player.getLocation())+90.0f;
    }

    public static float getBeta2(Entity entity, Player player) {
        /*Location A = entity.getLocation();
        double XA = A.getX();
        double YA = A.getY();
        YA = YA + 2;
        double ZA = A.getZ();

        Location B = player.getLocation();
        double XB = B.getX();
        double YB = B.getY();
        YB = YB + 1;
        double ZB = B.getZ();

        double alpha2 = Math.toDegrees(Math.atan(((YB-YA)/(Math.sqrt(((XB-XA)*(XB-XA))+((ZB-ZA)*(ZB-ZA)))))));

        float beta2 = A.getPitch();

        if (YB == YA) {
            beta2 = 0;
        } else if (YB >= YA) {
            beta2 = (float) -alpha2;
        } else if (YB <= YA) {
            beta2 = (float) -alpha2;
        }

        if (beta2 > 180) {
            beta2 = 180;
        } else if (beta2 < -180) {
            beta2 = -180;
        }

        return beta2;*/
        return UtilVector.getPitchLocations(entity.getLocation(), player.getLocation());
        //return getLookAtPitch(entity.getLocation(), player.getLocation());
    }

}
