package com.masterpik.hub.confy;

import com.masterpik.api.util.UtilArrayList;
import com.masterpik.hub.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class AnimalsConfy {

    public static void animalsInit() {

        File file = new File(SettingsConfy.animalsConfyPath);

        if (!file.exists())

        {
            ArrayList<String> animalsList = new ArrayList<String>();
            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
            String example1 = "example1";

            fileConfig.createSection("Animals");

            /*animalsList.add(example1);
            fileConfig.set("Animals.list", animalsList);

            ArrayList<String> list0 = new ArrayList<String>();
            list0.add("l1");
            list0.add("l2");
            list0.add("l3");
            fileConfig.set("Animals."+example1+".confyName", example1);
            fileConfig.set("Animals."+example1+".holoName", list0);
            fileConfig.set("Animals."+example1+".item", "STONE");

            fileConfig.set("Animals." + example1 + ".location.x", (double) 5);
            fileConfig.set("Animals." + example1 + ".location.y", (double) 64);
            fileConfig.set("Animals." + example1 + ".location.z", (double) 254);
            fileConfig.set("Animals."+example1+".location.yaw", "EAST");

            fileConfig.set("Animals."+example1+".menu.total", 2);

            fileConfig.set("Animals." + example1 + ".menu.main.cases", 9);
            fileConfig.set("Animals." + example1 + ".menu.main.displayName", example1);

            fileConfig.set("Animals."+example1+".menu.main.items.nb", 2);

            fileConfig.set("Animals."+example1+".menu.main.items.1.materialType", "STONE");
            fileConfig.set("Animals."+example1+".menu.main.items.1.data", 0);
            fileConfig.set("Animals."+example1+".menu.main.items.1.displayName", example1);
            fileConfig.set("Animals."+example1+".menu.main.items.1.place", 0);
            fileConfig.set("Animals."+example1+".menu.main.items.1.type", 0);
            ArrayList<String> list1 = new ArrayList<String>();
            list1.add("1");
            list1.add("3");
            fileConfig.set("Animals."+example1+".menu.main.items.1.link", list1);

            fileConfig.set("Animals."+example1+".menu.main.items.2.materialType", "GRASS");
            fileConfig.set("Animals."+example1+".menu.main.items.2.data", 0);
            fileConfig.set("Animals."+example1+".menu.main.items.2.displayName", example1);
            fileConfig.set("Animals."+example1+".menu.main.items.2.place", 2);
            fileConfig.set("Animals."+example1+".menu.main.items.2.type", 0);
            list1.clear();
            list1.add("2");
            list1.add("3");
            fileConfig.set("Animals."+example1+".menu.main.items.2.link", list1);


            fileConfig.set("Animals." + example1 + ".menu.1.cases", 18);

            fileConfig.set("Animals." + example1 + ".menu.1.displayName", example1);
            fileConfig.set("Animals."+example1+".menu.1.items.nb", 1);

            fileConfig.set("Animals."+example1+".menu.1.items.1.materialType", "DIRT");
            fileConfig.set("Animals."+example1+".menu.1.items.1.data", 0);
            fileConfig.set("Animals."+example1+".menu.1.items.1.displayName", example1);
            fileConfig.set("Animals."+example1+".menu.1.items.1.place", 5);
            fileConfig.set("Animals."+example1+".menu.1.items.1.type", 1);
            list1.clear();
            list1.add("rush");
            list1.add("1");
            list1.add("1");
            list1.add("1");
            fileConfig.set("Animals."+example1+".menu.1.items.1.link", list1);



            fileConfig.set("Animals." + example1 + ".menu.2.cases", 18);

            fileConfig.set("Animals." + example1 + ".menu.2.displayName", example1);
            fileConfig.set("Animals."+example1+".menu.2.items.nb", 1);

            fileConfig.set("Animals."+example1+".menu.2.items.1.materialType", "WOOD");
            fileConfig.set("Animals."+example1+".menu.2.items.1.data", 0);
            fileConfig.set("Animals."+example1+".menu.2.items.1.displayName", example1);
            fileConfig.set("Animals."+example1+".menu.2.items.1.place", 4);
            fileConfig.set("Animals."+example1+".menu.2.items.1.type", 1);
            list1.clear();
            list1.add("tower");
            list1.add("3");
            fileConfig.set("Animals."+example1+".menu.2.items.1.link", list1);*/




            try {
                fileConfig.save(file);
                Bukkit.getLogger().info("Configuration File Created");
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

    public static ArrayList<String> getList() {

        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        ArrayList<String> animalsList = new ArrayList<String>();

        animalsList.addAll((ArrayList<String>) fileConfig.getList("Animals.list"));

        return animalsList;
    }

    public static String getName(String confyName) {
        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        String name = fileConfig.getString("Animals."+confyName+".confyName");

        return name;
    }

    public static MaterialData getItem(String confyName) {
        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        MaterialData item = new MaterialData(Material.matchMaterial(fileConfig.getString("Animals."+confyName+".item")), (byte) fileConfig.getInt("Animals."+confyName+".data"));

        return item;
    }

    public static Location getLocation(String confyName) {
        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        Location location = new Location(GeneralConfy.getHubWorld(),
                fileConfig.getDouble("Animals."+confyName+".location.x"),
                fileConfig.getDouble("Animals."+confyName+".location.y"),
                fileConfig.getDouble("Animals."+confyName+".location.z"),
                MiscLocations.directionToYaw(BlockFace.valueOf(fileConfig.getString("Animals."+confyName+".location.yaw"))),
                (float) fileConfig.getDouble("Animals."+confyName+".location.pitch"));

        return location;
    }

    public static boolean getVisible(String confyName) {
        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
        return fileConfig.getBoolean("Animals."+confyName+".isVisible");
    }

    public static ArrayList<Inventory> getMenu(String confyName) {
        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        int nbInventory = 0;
        int nbItemsMain = 0;
        int bucle = 1;
        int nbItem = 0;
        int bucle2 = 1;


        ArrayList<Inventory> invs = new ArrayList<Inventory>();

        //main

        Inventory inventoryMain = Bukkit.createInventory(null,
                fileConfig.getInt("Animals." + confyName + ".menu.main.cases"),
                fileConfig.getString("Animals."+confyName+".menu.main.displayName"));

        nbItemsMain = fileConfig.getInt("Animals."+confyName+".menu.main.items.nb");


        bucle = 1;
        while (bucle <= nbItemsMain) {

            int type = fileConfig.getInt("Animals."+confyName+".menu.main.items."+bucle+".type");

            ItemStack item;

            if (type == 4) {
                item = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

                SkullMeta itemMeta = (SkullMeta) item.getItemMeta();
                itemMeta.setDisplayName(fileConfig.getString("Animals." + confyName + ".menu.main.items." + bucle + ".displayName"));
                itemMeta.setLore((List<String>) fileConfig.getList("Animals." + confyName + ".menu.main.items." + bucle + ".displayLore"));
                itemMeta.setOwner("MHF_Enderman");
                item.setItemMeta(itemMeta);

                //Bukkit.getLogger().info(item.toString());

            } else {

                item = /*NbtFactory.getCraftItemStack(*/new ItemStack(
                        Material.valueOf(fileConfig.getString("Animals." + confyName + ".menu.main.items." + bucle + ".materialType")),
                        fileConfig.getInt("Animals." + confyName + ".menu.main.items." + bucle + ".nb"),
                        (short) 0,
                        (byte) fileConfig.getInt("Animals." + confyName + ".menu.main.items." + bucle + ".data"))/*)*/;

                ItemMeta itemMeta = item.getItemMeta();

                if (item.getType().equals(Material.ELYTRA)) {
                    itemMeta.spigot().setUnbreakable(true);
                }

                itemMeta.setDisplayName(fileConfig.getString("Animals." + confyName + ".menu.main.items." + bucle + ".displayName"));
                ArrayList<String> lore = new ArrayList<>();
                if (fileConfig.getList("Animals." + confyName + ".menu.main.items." + bucle + ".displayLore") != null) {
                    lore.addAll((Collection<? extends String>) fileConfig.getList("Animals." + confyName + ".menu.main.items." + bucle + ".displayLore"));
                    //itemMeta.setLore((List<String>) fileConfig.getList("Animals." + confyName + ".menu.main.items." + bucle + ".displayLore"));
                }
                /*if (type == 1) {
                    lore.add(" ");
                    lore.add("§a- clique pour");
                    lore.add("§a§n  jouer !    ");
                }*/
                itemMeta.setLore(lore);
                item.setItemMeta(itemMeta);

            }

            //NbtFactory.NbtCompound itemComp = NbtFactory.fromItemTag(item);
            //itemComp.putPath("display.Name", fileConfig.getString("Animals." + confyName + ".menu.main.items." + bucle + ".displayName"));
            //NbtFactory.Wrapper list = (NbtFactory.Wrapper) fileConfig.getList("Animals." + confyName + ".menu.main.items." + bucle + ".displayLore");
            //itemComp.putPath("display.Lore", list);



            inventoryMain.setItem(fileConfig.getInt("Animals." + confyName + ".menu.main.items." + bucle + ".place"), item);

            boolean isCount = false;

            if (fileConfig.contains("Animals."+confyName+".menu.main.items."+bucle+".isCount")) {
                isCount = fileConfig.getBoolean("Animals."+confyName+".menu.main.items."+bucle+".isCount");
            } else {
                isCount = false;
            }

            boolean isCountSame = false;

            if (fileConfig.contains("Animals."+confyName+".menu.main.items."+bucle+".isCountSame")) {
                isCountSame = fileConfig.getBoolean("Animals."+confyName+".menu.main.items."+bucle+".isCountSame");
            } else {
                isCountSame = false;
            }

            ArrayList<String> countLink;

            if (isCount) {
                if (isCountSame) {
                    countLink = (ArrayList<String>) fileConfig.getList("Animals."+confyName+".menu.main.items."+bucle+".link");
                } else {
                    countLink = (ArrayList<String>) fileConfig.getList("Animals." + confyName + ".menu.main.items." + bucle + ".countLink");
                }

            } else {
                countLink = new ArrayList<>();
            }

            Item clickItem = new Item(confyName,
                    item,
                    type,
                    (ArrayList<String>) fileConfig.getList("Animals."+confyName+".menu.main.items."+bucle+".link"),
                    isCount,
                    countLink);

            if(isCount) {
                String countLinkStr = UtilArrayList.ArrayListToCryptedString(countLink);
                Main.PlayersCS.put(countLinkStr, new CountServ(countLinkStr, clickItem));
            }

            Main.menuItems.add(clickItem);
            bucle ++;
        }

        invs.add(inventoryMain);

        //others

        bucle = 1;

        nbInventory = fileConfig.getInt("Animals."+confyName+".menu.total");

        while (bucle <= nbInventory) {

            bucle2 = 1;

            Inventory threadInventory = Bukkit.createInventory(null,
                    fileConfig.getInt("Animals."+confyName+".menu."+bucle+".cases"),
                    fileConfig.getString("Animals."+confyName+".menu."+bucle+".displayName"));

            nbItem = fileConfig.getInt("Animals."+confyName+".menu."+bucle+".items.nb");

            while (bucle2 <= nbItem) {

                int type = fileConfig.getInt("Animals."+confyName+".menu."+bucle+".items."+bucle2+".type");

                ItemStack item;

                if (type == 4) {
                    item = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());

                    SkullMeta itemMeta = (SkullMeta) item.getItemMeta();
                    itemMeta.setDisplayName(fileConfig.getString("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayName"));
                    itemMeta.setLore((List<String>) fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayLore"));
                    itemMeta.setOwner("MHF_Enderman");
                    item.setItemMeta(itemMeta);


                } else {

                    item = /*NbtFactory.getCraftItemStack(*/new ItemStack(
                            Material.valueOf(fileConfig.getString("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".materialType")),
                            fileConfig.getInt("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".nb"),
                            (short) 0,
                            (byte) fileConfig.getInt("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".data"))/*)*/;

                    ItemMeta itemMeta = item.getItemMeta();

                    if (item.getType().equals(Material.ELYTRA)) {
                        itemMeta.spigot().setUnbreakable(true);
                    }

                    itemMeta.setDisplayName(fileConfig.getString("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayName"));

                    ArrayList<String> lore = new ArrayList<>();
                    //lore.addAll(itemMeta.getLore());
                    if (fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayLore") != null) {
                        lore.addAll((Collection<? extends String>) fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayLore"));
                    }
                    //itemMeta.setLore((List<String>) fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayLore"));
                    /*if (type == 1) {
                        lore.add(" ");
                        lore.add("§a- clique pour");
                        lore.add("§a§n  jouer !    ");
                    }*/
                    itemMeta.setLore(lore);
                    item.setItemMeta(itemMeta);

                }




                //NbtFactory.NbtCompound itemComp = NbtFactory.fromItemTag(item);
                //itemComp.putPath("display.Name", fileConfig.getString("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".displayName"));
                //itemComp.putPath("display.Lore", fileConfig.getList("Animals."+confyName+".menu."+bucle+".items."+bucle2+".displayLore"));

                threadInventory.setItem(fileConfig.getInt("Animals."+confyName+".menu."+bucle+".items."+bucle2+".place"), item);

                boolean isCount = false;

                if (fileConfig.contains("Animals."+confyName+".menu."+bucle+".items."+bucle2+".isCount")) {
                    isCount = fileConfig.getBoolean("Animals."+confyName+".menu."+bucle+".items."+bucle2+".isCount");
                } else {
                    isCount = false;
                }

                boolean isCountSame = false;

                if (fileConfig.contains("Animals."+confyName+".menu."+bucle+".items."+bucle2+".isCountSame")) {
                    isCountSame = fileConfig.getBoolean("Animals."+confyName+".menu."+bucle+".items."+bucle2+".isCountSame");
                } else {
                    isCountSame = false;
                }

                ArrayList<String> countLink;

                if (isCount) {
                    if (isCountSame) {
                        countLink = (ArrayList<String>) fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." +bucle2+".link");
                    } else {
                        countLink = (ArrayList<String>) fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." + bucle2 + ".countLink");
                    }

                } else {
                    countLink = new ArrayList<>();
                }

                Item clickItem = new Item(confyName,
                        item,
                        type,
                        (ArrayList<String>) fileConfig.getList("Animals." + confyName + ".menu." + bucle + ".items." +bucle2+".link"),
                        isCount,
                        countLink);

                Main.menuItems.add(clickItem);

                if(isCount) {
                    String countLinkStr = UtilArrayList.ArrayListToCryptedString(countLink);
                    Main.PlayersCS.put(countLinkStr, new CountServ(countLinkStr, clickItem));
                }

                bucle2 ++;
            }

            invs.add(threadInventory);

            bucle++;
        }

        return invs;
    }

    public static ArrayList<String> getNameLine(String confyName) {
        File file = new File(SettingsConfy.animalsConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        ArrayList<String> listName = (ArrayList<String>) fileConfig.getList("Animals."+confyName+".holoName");

        return listName;
    }

}
