package com.masterpik.hub.confy;

import com.masterpik.hub.MiscLocations;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class GeneralConfy {

    public static void generalInit() {
        File file = new File(SettingsConfy.generalConfyPath);

        if (!file.exists()) {
            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

            fileConfig.createSection("General");

            fileConfig.set("General.location.world", "world");
            Location location = Bukkit.getWorld("world").getSpawnLocation();
            fileConfig.set("General.location.x", location.getX());
            fileConfig.set("General.location.y", location.getY());
            fileConfig.set("General.location.z", location.getZ());
            fileConfig.set("General.location.yaw", "NORTH");
            fileConfig.set("General.location.pitch", 0);
            fileConfig.set("General.minY", 10);

            try {
                fileConfig.save(file);
                Bukkit.getLogger().info("Configuration File Created");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    public static World getHubWorld() {
        File file = new File(SettingsConfy.generalConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        World world = Bukkit.getWorld(fileConfig.getString("General.location.world"));

        return world;
    }

    public static Location getSpawnHubLocation() {
        File file = new File(SettingsConfy.generalConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        Location location = new Location(GeneralConfy.getHubWorld(),
                fileConfig.getDouble("General.location.x"),
                fileConfig.getDouble("General.location.y"),
                fileConfig.getDouble("General.location.z"),
                MiscLocations.directionToYaw(BlockFace.valueOf(fileConfig.getString("General.location.yaw"))),
                fileConfig.getInt("General.location.ptich"));

        return location;
    }

    public static int getMinimumY() {
        File file = new File(SettingsConfy.generalConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
        int minY = fileConfig.getInt("General.minY");
        return minY;
    }

}
