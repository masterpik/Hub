package com.masterpik.hub.confy;

import com.masterpik.hub.Item;
import com.masterpik.hub.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PlayerInventoryConfy {

    public static void generalInit() {
        File file = new File(SettingsConfy.playerInventoryConfyPath);

        if (!file.exists()) {
            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

            fileConfig.createSection("PlayerInventory");


            try {
                fileConfig.save(file);
                Bukkit.getLogger().info("Configuration File Created");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }

    public static Inventory getInventory() {
        File file = new File(SettingsConfy.playerInventoryConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        Inventory inventory = Bukkit.createInventory(null, InventoryType.PLAYER);

        int bucle1 = 0;

        int max = 0;

        if (fileConfig.contains("PlayerInventory.nbItems")) {
            max = fileConfig.getInt("PlayerInventory.nbItems");
        }

        while (bucle1 < max) {

            ItemStack item = new ItemStack(Material.valueOf(fileConfig.getString("PlayerInventory."+bucle1+".materialType")),
                    fileConfig.getInt("PlayerInventory."+bucle1+".nb"),
                    (short) 0,
                    (byte) fileConfig.getInt("PlayerInventory."+bucle1+".data"));

            ItemMeta itemMeta = item.getItemMeta();
            itemMeta.setDisplayName(fileConfig.getString("PlayerInventory."+bucle1+".displayName"));
            itemMeta.setLore((List<String>) fileConfig.getList("PlayerInventory." + bucle1 + ".displayLore"));
            item.setItemMeta(itemMeta);


            inventory.setItem(fileConfig.getInt("PlayerInventory."+bucle1+".place"), item);

            Item clickItem = new Item("PlayerInventory",
                    item,
                    fileConfig.getInt("PlayerInventory."+bucle1+".type"),
                    (ArrayList<String>) fileConfig.getList("PlayerInventory."+bucle1+".link"),
                    false,
                    new ArrayList<>());
            Main.menuItems.add(clickItem);

            bucle1++;
        }

        return inventory;

    }

}
