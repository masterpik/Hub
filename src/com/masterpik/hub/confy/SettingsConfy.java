package com.masterpik.hub.confy;

public class SettingsConfy {

    public static String animalsConfyPath = "plugins/Hub/animals.yml";
    public static String generalConfyPath = "plugins/Hub/general.yml";
    public static String playerInventoryConfyPath = "plugins/Hub/playerInventory.yml";

}
