package com.masterpik.hub;

import com.masterpik.api.nms.entity.CustomEntityEnderman;
import com.masterpik.api.specialEffects.Fireworks;
import com.masterpik.api.util.UtilEntity;
import com.masterpik.hub.confy.AnimalsConfy;
import com.masterpik.hub.confy.GeneralConfy;
import com.sainttx.holograms.HologramManager;
import com.sainttx.holograms.HologramPlugin;
import com.sainttx.holograms.api.HologramLine;
import com.sainttx.holograms.api.NMSEntityBase;
import com.sainttx.holograms.internal.HologramImpl;
import com.sainttx.holograms.internal.HologramLineImpl;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_9_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_9_R1.entity.CraftEntity;
import org.bukkit.entity.*;
import org.bukkit.material.MaterialData;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.Iterator;

public class AnimalsManagement {

    public static void spawnAll() {
        int bucle1 = 0;

        ArrayList<String> animals = AnimalsConfy.getList();

        while (bucle1 < animals.size()) {

            if (AnimalsConfy.getVisible(animals.get(bucle1))) {

                /*ArmorStand stand = (ArmorStand) GeneralConfy.getHubWorld().spawnEntity(AnimalsConfy.getLocation(animals.get(bucle1)).add(0,-2,0), EntityType.ARMOR_STAND);

                stand.setGravity(false);
                stand.setVisible(false);
                stand.setCanPickupItems(false);
                stand.setRemoveWhenFarAway(false);
                stand.setCustomName(AnimalsConfy.getName(animals.get(bucle1)+"|STAND"));
                stand.setCustomNameVisible(false);*/

                //Enderman entity = (Enderman) GeneralConfy.getHubWorld().spawnEntity(AnimalsConfy.getLocation(animals.get(bucle1)), EntityType.ENDERMAN);

                CustomEntityEnderman e = new CustomEntityEnderman(((CraftWorld)GeneralConfy.getHubWorld()).getHandle());

                Enderman entity = e.spawn(AnimalsConfy.getLocation(animals.get(bucle1)));

                //((CraftWorld)GeneralConfy.getHubWorld()).getHandle().addEntity(e);
                //e.teleportTo(AnimalsConfy.getLocation(animals.get(bucle1)), false);
                //Enderman entity = (Enderman) ((CraftEntity) e.getBukkitEntity());

                /*stand.setSmall(true);

                stand.setPassenger(entity);*/

                entity.setCustomName(AnimalsConfy.getName(animals.get(bucle1)));
                entity.setCustomNameVisible(false);

                entity.setCanPickupItems(false);
                entity.setRemoveWhenFarAway(false);

                entity.setCarriedMaterial(AnimalsConfy.getItem(animals.get(bucle1)));

                entity.setMetadata("hub", new FixedMetadataValue(Bukkit.getPluginManager().getPlugin("Hub"), true));
                entity.setMetadata("name", new FixedMetadataValue(Bukkit.getPluginManager().getPlugin("Hub"), animals.get(bucle1)));

                AnimalsManagement.initEntity(entity);


                AnimalsManagement.addHologram(AnimalsConfy.getLocation(animals.get(bucle1)), animals.get(bucle1));
            }

            Main.inventoriesMap.put(animals.get(bucle1), AnimalsConfy.getMenu(animals.get(bucle1)));

            bucle1++;
        }

    }

    public static void addHologram(Location location, String customName) {

        /*Hologram holo = HologramsAPI.createHologram(Main.holoPlugin, location);
        holo.clearLines();

        //Bukkit.getLogger().info(customName);

        ArrayList<String> list = AnimalsConfy.getNameLine(customName);



        //Bukkit.getLogger().info(Integer.toString(list.size()));

        int bucle1 = 0;
        while (bucle1 < list.size()) {

            holo.insertTextLine(bucle1, list.get(bucle1));

            bucle1++;
        }

        Location newLocation = location.clone();
        double newnb = list.size()/3.5;

        newLocation.add(0, 3, 0);
        newLocation.add(0, newnb, 0);

        holo.teleport(newLocation);

        Main.holos.add(holo);*/

        ArrayList<String> list = AnimalsConfy.getNameLine(customName);

        HologramManager hm = HologramManager.getInstance();

        if (hm.getHologramByName(customName) != null) {
            hm.getHologramByName(customName).despawn();
            hm.getHologramByName(customName).remove();
        }

        if (hm.getActiveHolograms().containsKey(customName)) {
            hm.getHologramByName(customName).despawn();
            hm.getHologramByName(customName).remove();
        }

        Location newLocation = location.clone();
        double newnb = list.size()/3.5;

        newLocation.add(0, 2.5, 0);
        newLocation.add(0, newnb, 0);

        HologramImpl holo = new HologramImpl(customName, newLocation, true, list.get(0));


        int bucle1 = 1;
        while (bucle1 < list.size()) {

            holo.addLine(new HologramLineImpl(holo, list.get(bucle1)));

            bucle1++;
        }

        hm.saveHologram(holo);

        //holo.teleport(newLocation);


        hm.saveHologram(holo);

        Main.holos.add(holo);

        BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
            @Override
            public void run() {

                holo.refresh();

            }
        }, 10L);

    }

    public static void killAll(){

        ArrayList<Entity> all = (ArrayList<Entity>) GeneralConfy.getHubWorld().getEntities();

        if (!all.isEmpty()) {

            int bucle = 0;

            while (bucle < all.size()) {

                if (all.get(bucle).hasMetadata("hub")) {
                    if ((all.get(bucle).getMetadata("hub").get(0).asBoolean())) {
                        all.get(bucle).remove();
                    }
                }

                bucle++;
            }
        }



    }

    public static void killHolos() {
        int bucle2 = 0;

        while (bucle2 < Main.holos.size()) {

            //Main.holos.get(bucle2).delete();

            Main.holos.get(bucle2).despawn();

            Main.holos.get(bucle2).remove();

            //HologramsAPI.getHolograms(Main.holoPlugin).clear();

            //HologramsAPI.getHolograms(Main.holoPlugin).remove(Main.holos.get(bucle2));

            bucle2++;
        }
    }

    public static void initEntity(Entity entity) {
        //Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "entitydata " + entity.getUniqueId() + " {NoAI:1, Invulnerable:1, Silent:1, ActiveEffects:[{Id:25,Duration:999999999,Amplifier:-1}]}");

        UtilEntity.setNBTtagBoolean(entity, "NoAI", 1);
        UtilEntity.setNBTtagBoolean(entity, "Silent", 1);
        UtilEntity.setNBTtagBoolean(entity, "Invulnerable", 1);

        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "entitydata " + entity.getUniqueId() + " {Silent:1, ActiveEffects:[{Id:25,Duration:999999999,Amplifier:-1}]}");

        /*UtilEntity.setAiEnabled(entity, false);
        UtilEntity.setVulnerableEnabled(entity, false);
        UtilEntity.setNoSilentEnabled(entity, false);*/
    }

    @Deprecated
    public static void initEntityAI(Entity entity) {
        //Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "entitydata " + entity.getUniqueId() + " {NoAI:0, Invulnerable:1, Silent:1, ActiveEffects:[{Id:25,Duration:999999999,Amplifier:-1}]}");
    }


}
