package com.masterpik.hub.scoreboard;

import com.masterpik.messages.spigot.Api;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class ScorebaordManagement {

    public static Scoreboard getScoreboard(Player player) {
        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        scoreboard.registerNewTeam(player.getName());
        scoreboard.getTeam(player.getName()).setOption(Team.Option.COLLISION_RULE, Team.OptionStatus.NEVER);
        scoreboard.getTeam(player.getName()).addEntry(player.getName());

        return scoreboard;
    }

}
