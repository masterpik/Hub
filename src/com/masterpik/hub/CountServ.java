package com.masterpik.hub;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class CountServ {

    String name;
    int count;
    Item item;

    public CountServ(String name, Item item) {
        this.name = name;
        this.item = item;
        this.count = 0;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }
    public void setCount(int Pcount) {
        this.count = Pcount;
    }

    public Item getItem() {
        return item;
    }
    public void setItem(Item item) {
        this.item = item;
    }

    public void refreshCount() {
        ItemStack stack = this.item.getItemStack().clone();
        ItemMeta meta = stack.getItemMeta();
        List<String> lores = new ArrayList<>();
        if (meta.hasLore()) {
            lores.addAll(meta.getLore());
        }
        String j = "§e§ojoueurs §r§b";

        if (this.item.getType() == 1) {
            if (lores.get(lores.size()-1).contains("clique")) {
                lores.remove(lores.size()-1);
            }
        }

        if (lores != null && lores.size() > 0) {
            if (lores.get(lores.size()-1).contains(j)) {
                if (!lores.get(lores.size()-1).contains(Integer.toString(this.count))) {
                    lores.remove(lores.size() - 1);
                } else {
                    return;
                }
            } else {
                lores.add(lores.size(), " ");
            }
        } else {
            lores.add(0, " ");
        }

        lores.add(lores.size(), j+this.count);
        if (this.item.getType() == 1) {
            lores.add("§a§nclique pour jouer !");
        }
        meta.setLore(lores);
        stack.setItemMeta(meta);

        int bucle = 0;

        while (bucle < Main.inventoriesMap.get(this.item.getAnimal()).size()) {

            if (Main.inventoriesMap.get(this.item.getAnimal()).get(bucle).contains(this.item.getItemStack())) {
                Main.inventoriesMap.get(this.item.getAnimal()).get(bucle).setItem(Main.inventoriesMap.get(this.item.getAnimal()).get(bucle).first(this.item.getItemStack()), stack);
            }

            bucle ++;
        }

        this.item.setItemStack(stack);
    }
}
