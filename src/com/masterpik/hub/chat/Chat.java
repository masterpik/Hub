package com.masterpik.hub.chat;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.masterpik.api.json.*;
import com.masterpik.api.players.MasterpikPlayer;
import com.masterpik.api.players.Players;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilColor;
import com.masterpik.api.util.UtilJson;
import net.md_5.bungee.api.chat.ComponentBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

public class Chat {

    public static void sendToList(String message, Collection<? extends Player> list) {
        int bucle = 0;

        ArrayList<Player> players = new ArrayList<>();

        players.addAll(list);


        PacketContainer chat = new PacketContainer(PacketType.Play.Server.CHAT);
        chat.getChatComponents().write(0, WrappedChatComponent.fromJson(message));


        while (bucle < players.size()) {

            //TODO finish function

            try {
                ProtocolLibrary.getProtocolManager().sendServerPacket(players.get(bucle), chat);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

            bucle ++;
        }

    }

    public static void sendAMessage(Player player, String message) {
        message = transformMessage(player, message);

        //player.sendMessage(message);
        sendToList(message, Bukkit.getOnlinePlayers());

    }

    public static String transformMessage(Player player, String message) {
        message = addPlayerName(player, message);

        MasterpikPlayer Mplayer = Players.players.get(player.getPlayer().getUniqueId());

        //message = "[\"\",{\"text\":\""+player.getName()+" \",\"color\":\""+ UtilColor.chatColorStringToColorName(Mplayer.getStatu().getColor())+"\",\"bold\":true,\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"/msg "+player.getName()+" \"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":{\"text\":\"\",\"extra\":[{\"text\":\""+Mplayer.getStatu().getName()+"\",\"color\":\""+UtilColor.chatColorStringToColorName(Mplayer.getStatu().getColor())+"\",\"bold\":true}]}}},{\"text\":\"-> "+message+"\",\"color\":\"yellow\",\"bold\":false}]";

        String prefix = "§"+Mplayer.getStatu().getColor().getCode()+"§l";

        if (Mplayer.getStatu().isNameVisible()) {
            prefix = prefix + "§o/" + Mplayer.getStatu().getName()+"/ "+"§r§"+Mplayer.getStatu().getColor().getCode()+"§l";
        }

        ChatText msg1 = new ChatText(prefix+player.getName()).setColor(Mplayer.getStatu().getColor()).setItalic(true).setBold(true);
        msg1 = msg1.setClickEvent(new ClickEvent(ClickEvents.SUGGEST_COMMAND, "/msg "+player.getName()+" "));
        if (true/*Mplayer.getStatu().isNameVisible()*/) {
            msg1 = msg1.setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT, new ChatText(Mplayer.getStatu().getName()).setColor(Mplayer.getStatu().getColor()).setBold(true)));
        }

        /*ChatText msg1o = new ChatText(
                player.getName(),
                Colors.valueOf(UtilColor.chatColorStringToColorName(Mplayer.getStatu().getColor())),
                true,
                false,
                false,
                false,
                false,
                null,
                new ClickEvent(
                        ClickEvents.SUGGEST_COMMAND,
                        "/msg "+player.getName()+" "),
                new HoverEvent(
                        HoverEvents.SHOW_TEXT,
                        new ChatText(
                                Mplayer.getStatu().getName(),
                                Colors.valueOf(UtilColor.chatColorStringToColorName(Mplayer.getStatu().getColor())),
                                true,
                                false,
                                false,
                                false,
                                false)));*/

        ChatText msg2 = new ChatText(" ➤ ").setColor(Colors.GOLD);
        ChatText msg3 = new ChatText(message).setColor(Colors.WHITE);

        Object[] msgs = {"", msg1,msg2, msg3};


        message = UtilJson.getJsonFromObject(msgs);

        return message;
    }

    public static String addPlayerName(Player player, String message) {
        return message;
    }

}
