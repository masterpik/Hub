package com.masterpik.hub;

import com.masterpik.api.Networks.DatasPing;
import com.masterpik.api.Networks.Servers;
import com.masterpik.api.util.UtilNetwork;
import com.masterpik.api.util.UtilString;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class CountServManagement extends BukkitRunnable {

    private static BukkitRunnable runnable;

    @Override
    public void run() {

        ArrayList<CountServ> list = new ArrayList<>();
        list.addAll(Main.PlayersCS.values());

        int bucle = 0;

        while (bucle < list.size()) {

            if (list.get(bucle).getCount() >= 1 && list.get(bucle).getCount() <= 10) {

                if (UtilNetwork.serverIsOnline(Servers.valueOf(UtilString.CryptedStringToArrayList(list.get(bucle).getName()).get(0).toUpperCase()))
                        && Integer.parseInt(
                            UtilNetwork.getDatasServer(
                                    Servers.valueOf(UtilString.CryptedStringToArrayList(list.get(bucle).getName()).get(0).toUpperCase()),
                                    DatasPing.ONLINE_PLAYERS)) == 0) {
                    list.get(bucle).setCount(0);
                } else if (!UtilNetwork.serverIsOnline(Servers.valueOf(UtilString.CryptedStringToArrayList(list.get(bucle).getName()).get(0).toUpperCase()))) {
                    list.get(bucle).setCount(0);
                }
            }

            list.get(bucle).refreshCount();


            bucle++;
        }

    }

    public static void CountServInit() {
        runnable = new CountServManagement();
    }

    public static void startTimer(int second) {
        runnable.runTaskTimer(Main.plugin, 0, (second*20));
    }

    public static void stopTimer() {
        runnable.cancel();
    }

}
