package com.masterpik.hub;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import com.masterpik.api.util.UtilArrayList;
import com.masterpik.api.util.UtilString;
import com.masterpik.hub.confy.GeneralConfy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.plugin.messaging.PluginMessageRecipient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

public class BungeeWork implements PluginMessageListener{



    public static void sendPlayer(Player player, String serverName) {
        /*ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(serverName);
        player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());*/

        final BungeeUtils bungee = new BungeeUtils(Main.plugin, "gamesCo");
        try {
            bungee.sendRequest(BungeeUtils.MessageType.CONNECT, serverName, null, null, "gamesCo", player);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static void sendToGetPlayerNb(Player player, String server) {
        try {
            Main.bungee.sendRequest(BungeeUtils.MessageType.PLAYER_COUNT, server, null, null, null, player);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendToGameMessage(Player player, ArrayList<String> arguments) {

        /*ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("PlayerCount");
        out.writeUTF("ALL");

        int bucle = 0;
        while (bucle < arguments.size()) {
            out.writeUTF(arguments.get(bucle));
            bucle++;
        }


        player.sendPluginMessage(Main.plugin, "BungeeCord", out.toByteArray());*/

        String argumentsString = Utils.ArrayToString(arguments);

        try {
            Main.bungee.sendRequest(BungeeUtils.MessageType.FORWARD, arguments.get(0), null, argumentsString, "gamesCo", Bukkit.getServer());
            Bukkit.getLogger().info("MESSAGE ENVOY2");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    Plugin plugin;

    public BungeeWork(Plugin instance) {
        plugin = instance;
    }
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        Bukkit.getLogger().info("TA RECU UN SMS X");
        if (Objects.equals(channel, "BungeeCord")) {
            ByteArrayDataInput in = ByteStreams.newDataInput(message);
            String subchannel = in.readUTF();

            String getMessage = in.readUTF();

            if (subchannel == "bungeeUtilTeleportHub") {
                //String getMessage = in.readUTF();
                Bukkit.getPlayer(getMessage).teleport(GeneralConfy.getSpawnHubLocation());
            } else if (Objects.equals(subchannel, "hubCo")) {
                Bukkit.getLogger().info("Message de la hubCo : " + getMessage);

                ArrayList<String> data = UtilString.CryptedStringToArrayList(getMessage);

                int bcl = 1;
                while (bcl < data.size()){

                    ArrayList<String> list = new ArrayList<>();
                    list.addAll(data.subList(1,bcl+1));
                    String lk = UtilArrayList.ArrayListToCryptedString(list);

                    if (Main.PlayersCS.containsKey(lk)) {
                        if (Main.PlayersCS.get(lk).getCount() > 0) {
                            Main.PlayersCS.get(lk).setCount(Main.PlayersCS.get(lk).getCount() - 1);
                        } else {
                            Main.PlayersCS.get(lk).setCount(0);
                        }
                    }

                    bcl++;
                }

            }
        }
    }
}
