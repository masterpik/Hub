package com.masterpik.hub;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;

public class MiscLocations {

    public static float directionToYaw(BlockFace face) {
        float yaw = 0;

        if (face.equals(BlockFace.NORTH)) {
            yaw = 180;
        }
        else if (face.equals(BlockFace.EAST)) {
            yaw = -90;
        }
        else if (face.equals(BlockFace.SOUTH)) {
            yaw = 0;
        }
        else if (face.equals(BlockFace.WEST)) {
            yaw = 90;
        }

        return yaw;
    }

    public static Location directionToYaw(Location location, int dif) {
        Location loc = location.clone();
        Float yaw = location.getYaw();

        if (yaw == 180) {
            loc.setZ(location.getZ()-dif);
        }
        else if (yaw == -90) {
            loc.setX(location.getX() - dif);
        }
        else if (yaw == 0) {
            loc.setZ(location.getZ()+dif);
        }
        else if (yaw == 90) {
            loc.setX(location.getX()-dif);
        }

        loc.setYaw(location.getYaw()-180);
        loc.setPitch(0-location.getPitch());

        return loc;
    }

}
