package com.masterpik.hub;

import com.comphenix.packetwrapper.WrapperPlayServerEntityLook;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.masterpik.api.PacketWrapper.WrapperPlayServerEntityHeadRotation;
import com.masterpik.api.json.ChatText;
import com.masterpik.api.nms.player.Title;
import com.masterpik.api.players.MasterpikPlayer;
import com.masterpik.api.players.Players;
import com.masterpik.api.specialEffects.Fireworks;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilArrayList;
import com.masterpik.api.util.UtilChat;
import com.masterpik.api.util.UtilEntity;
import com.masterpik.api.util.UtilTitle;
import com.masterpik.database.api.Query;
import com.masterpik.hub.chat.Chat;
import com.masterpik.hub.confy.AnimalsConfy;
import com.masterpik.hub.confy.GeneralConfy;
import com.masterpik.hub.scoreboard.ScorebaordManagement;
import com.masterpik.messages.spigot.Api;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Boat;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.*;
import org.bukkit.event.player.*;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

public class Events implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteract(PlayerInteractAtEntityEvent event) {
        Player player = event.getPlayer();
        Entity entity = event.getRightClicked();

        ArrayList<String> animals = AnimalsConfy.getList();

        if (entity.hasMetadata("name")) {
            player.openInventory(Main.inventoriesMap.get(entity.getMetadata("name").get(0).asString()).get(0));
            event.setCancelled(true);
        }

    }

    /*@EventHandler(priority = EventPriority.HIGHEST)
    public void InventoryDragEvent(InventoryDragEvent event) {
        if (event.getWhoClicked().getWorld().equals(GeneralConfy.getHubWorld()) && !event.getWhoClicked().isOp()) {
            event.setCancelled(true);
        }
    }*/

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getInventory();
        ItemStack item = event.getCurrentItem();

        if (player.getWorld().equals(GeneralConfy.getHubWorld())) {

        /*
        PROVISOIRE !!!!!!!
         */
        /**/
        /**/
        /**/
        /*
        PROVISOIRE !!!!!!!
         */
            if (!player.isOp()) {
                //event.setCancelled(true);
            }

            //Bukkit.getLogger().info("click inv");

            //Bukkit.getLogger().info(item.toString());

            int bucle = 0;
            while (bucle < Main.menuItems.size()) {

                // Bukkit.getLogger().info("click bcl");

                if (Main.menuItems.get(bucle).getItemStack().isSimilar(item)
                        || (item != null
                        && item.getType() !=null
                        && item.getType().equals(Material.SKULL_ITEM) && Main.menuItems.get(bucle).getItemStack().getType().equals(Material.SKULL_ITEM)
                            && item.getItemMeta().getDisplayName().equals(Main.menuItems.get(bucle).getItemStack().getItemMeta().getDisplayName()))) {
                    event.setCancelled(true);

                    //Bukkit.getLogger().info("type = "+Main.menuItems.get(bucle).getType());

                    if (Main.menuItems.get(bucle).getType() == 0) {
                        //Bukkit.getLogger().info("click type 0");
                        final int finalBucle = bucle;
                        BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(Main.inventoriesMap.get(Main.menuItems.get(finalBucle).getAnimal()).get(Integer.parseInt(Main.menuItems.get(finalBucle).getLink().get(0))));
                            }
                        }, 1);
                    } else if (Main.menuItems.get(bucle).getType() == 1) {
                        player.closeInventory();
                        //Bukkit.getLogger().info("click type 1");
                        ArrayList<String> finalLink = (ArrayList<String>) Main.menuItems.get(bucle).getLink().clone();

                        if (Main.menuItems.get(bucle).isCount()) {
                            ArrayList<String> countLink = Main.menuItems.get(bucle).getCountLink();

                            int bcl = 0;
                            while (bcl < countLink.size()){

                                ArrayList<String> list = new ArrayList<>();
                                list.addAll(countLink.subList(0,bcl+1));
                                String lk = UtilArrayList.ArrayListToCryptedString(list);

                                //Bukkit.getLogger().info(lk);

                                if (Main.PlayersCS.containsKey(lk)) {
                                    Main.PlayersCS.get(lk).setCount(Main.PlayersCS.get(lk).getCount() + 1);
                                }

                                bcl++;
                            }

                        }

                        finalLink.add(1, player.getName());
                        //BungeeWork.sendToGetPlayerNb(player, Main.menuItems.get(bucle).getLink().get(0));
                        BungeeWork.sendToGameMessage(player, finalLink);
                        BungeeWork.sendPlayer(player, finalLink.get(0));

                     /*final int finalBucle = bucle;
                    Bukkit.getScheduler().scheduleSyncDelayedTask(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            BungeeWork.sendToGameMessage(player, finalLink);
                        }
                    }, 10L);*/
                    } else if (Main.menuItems.get(bucle).getType() == 2) {
                        final int finalBucle = bucle;
                        BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.openInventory(Main.inventoriesMap.get(Main.menuItems.get(finalBucle).getLink().get(0)).get(0));
                            }
                        }, 1);
                    } else if (Main.menuItems.get(bucle).getType() == 3) {
                        ArrayList<String> link = (ArrayList<String>) Main.menuItems.get(bucle).getLink().clone();
                        player.closeInventory();
                        player.teleport(new Location(
                                Bukkit.getWorld(link.get(0)),
                                Double.parseDouble(link.get(1)),
                                Double.parseDouble(link.get(2)),
                                Double.parseDouble(link.get(3)),
                                Float.parseFloat(link.get(4)),
                                Float.parseFloat(link.get(5))));

                        player.getInventory().setItem(1,Main.menuItems.get(bucle).getItemStack().clone());

                        if (player.getInventory().getItem(1).getType().equals(Material.ELYTRA)) {
                            player.getInventory().setChestplate(Main.menuItems.get(bucle).getItemStack().clone());
                        }

                        BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.closeInventory();
                            }
                        }, 1);
                    } else if (Main.menuItems.get(bucle).getType() == 4) {
                        Location tp = AnimalsConfy.getLocation(Main.menuItems.get(bucle).getAnimal()).clone();

                        tp = MiscLocations.directionToYaw(tp, 3);

                        player.teleport(tp);

                    }

                    bucle = Main.menuItems.size()+1;

                }

                bucle++;
            }

        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onEntityDamage(EntityDamageEvent event) {
        Entity entity = event.getEntity();

        if (entity.getWorld().equals(GeneralConfy.getHubWorld())) {

            if (!event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) {
                event.setCancelled(true);
            }
        /*if (entity.hasMetadata("hub")) {
            if ((entity.getMetadata("hub").get(0).asBoolean())) {
                event.setCancelled(true);
            }
        }*/
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onEntityFire(EntityCombustEvent event) {
        if (event.getEntity().getWorld().equals(GeneralConfy.getHubWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onBlockFire(BlockBurnEvent event) {
        if (event.getBlock().getWorld().equals(GeneralConfy.getHubWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onHunger(FoodLevelChangeEvent event) {
        if (event.getEntity().getWorld().equals(GeneralConfy.getHubWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onExplode(EntityExplodeEvent event) {
        if (event.getEntity().getWorld().equals(GeneralConfy.getHubWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void PlayerDrop(PlayerDropItemEvent event) {
        if (event.getPlayer().getWorld().equals(GeneralConfy.getHubWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        if (player.getWorld().equals(GeneralConfy.getHubWorld())) {
            if (player.isOp() && !player.getGameMode().equals(GameMode.ADVENTURE)) {

            } else {
                event.setCancelled(true);
            }


            int bucle = 0;
            while (bucle < Main.menuItems.size()) {
                if (Main.menuItems.get(bucle).getItemStack().isSimilar(player.getInventory().getItemInMainHand())
                    && !event.getAction().equals(Action.PHYSICAL)) {
                    event.setCancelled(true);
                    if (Main.menuItems.get(bucle).getType() == 2) {
                        player.openInventory(Main.inventoriesMap.get(Main.menuItems.get(bucle).getLink().get(0)).get(0));
                    }
                    else if (Main.menuItems.get(bucle).getType() == 3) {
                        ArrayList<String> link = (ArrayList<String>) Main.menuItems.get(bucle).getLink().clone();
                        player.closeInventory();
                        player.teleport(new Location(
                                Bukkit.getWorld(link.get(0)),
                                Double.parseDouble(link.get(1)),
                                Double.parseDouble(link.get(2)),
                                Double.parseDouble(link.get(3)),
                                Float.parseFloat(link.get(4)),
                                Float.parseFloat(link.get(5))));

                        player.getInventory().setItem(1,Main.menuItems.get(bucle).getItemStack().clone());

                        if (player.getInventory().getItem(1).getType().equals(Material.ELYTRA)) {
                            player.getInventory().setChestplate(Main.menuItems.get(bucle).getItemStack().clone());
                        }

                        BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                player.closeInventory();
                            }
                        }, 1);

                    }
                }

                bucle++;
            }

        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();

        MasterpikPlayer Mplayer = Players.players.get(event.getPlayer().getUniqueId());

        player.teleport(GeneralConfy.getSpawnHubLocation());

        player.setGameMode(GameMode.ADVENTURE);

        String prefix = "§"+Mplayer.getStatu().getColor().getCode()+"§l";

        if (Mplayer.getStatu().isNameVisible()) {
            prefix = prefix + "§o/" + Mplayer.getStatu().getName()+"/ "+"§r§"+Mplayer.getStatu().getColor().getCode()+"§l";
        }

        player.setPlayerListName(prefix+player.getName());

        Utils.RealClear(player);

        Utils.seeAll(player);

        player.getInventory().setContents(Main.PlayerInventory.getContents());

        if (Mplayer != null
                && Mplayer.getStatu().isHubElytra()) {
            player.setAllowFlight(true);
        }

        if (Mplayer != null
                && Mplayer.getStatu().isShowHubCo()) {
            event.setJoinMessage(Main.masterpikPrefix+(Main.playerJoinMsg.replaceAll("%p", "§"+Mplayer.getStatu().getColor().getCode()+"§l"+player.getName())));
        } else {
            event.setJoinMessage("");
        }

        //UtilTitle.sendTitel(new Title(EnumWrappers.TitleAction.TITLE, new ChatText("§6§lBONJOUR ET BIENVENUE"), 1, 5, 1), player);
        //UtilChat.sendActionBar(new ChatText("§6§opour revenir au spawn, utilise /hub"), player);


        player.setScoreboard(ScorebaordManagement.getScoreboard(player));



    }

    @EventHandler(priority = EventPriority.HIGH)
    public void PlayerQuitEvent(PlayerQuitEvent event) {

        Player player = event.getPlayer();

        MasterpikPlayer Mplayer = Players.players.get(event.getPlayer().getUniqueId());

        if (Mplayer != null
                && Mplayer.getStatu().isShowHubCo()) {
            event.setQuitMessage(Main.masterpikPrefix+(Main.playerQuitMsg.replaceAll("%p", "§"+Mplayer.getStatu().getColor().getCode()+"§l"+player.getName())));
        } else {
            event.setQuitMessage("");
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();

        if (player.getWorld().equals(GeneralConfy.getHubWorld())
                || (player.getWorld().getName().equals("hub2") && player.getGameMode().equals(GameMode.ADVENTURE))) {


            Location at23 = player.getLocation().clone();
            at23.setY(23);
            Location at24 = player.getLocation().clone();
            at24.setY(24);
            Location at25 = player.getLocation().clone();
            at25.setY(25);
            Location at26 = player.getLocation().clone();
            at26.setY(26);

            if (player.getLocation().getBlockY() <= GeneralConfy.getMinimumY()
                    && !player.isOnGround()) {
                Location tp = GeneralConfy.getSpawnHubLocation();
                tp.setYaw(player.getLocation().getYaw());
                tp.setPitch(player.getLocation().getPitch());
                player.teleport(tp);
            }


            if (player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.SPONGE)) {
                if (player.getLocation().getBlock().getType().equals(Material.REDSTONE_WIRE)) {
                    player.setVelocity(player.getVelocity().setY(1));
                }
                else if (player.getLocation().getBlock().getType().equals(Material.IRON_PLATE)){
                    player.setVelocity((player.getLocation().getDirection().setY(0.8)));
                }
            }

            if ((player.isFlying() || player.isGliding()) && !player.getGameMode().equals(GameMode.CREATIVE)) {


                if (!at23.getBlock().getType().equals(Material.WATER) && !at23.getBlock().getType().equals(Material.STATIONARY_WATER)) {
                    if ((!at24.getBlock().getType().equals(Material.AIR) && !at24.getBlock().getType().equals(Material.BARRIER))
                            || (!at25.getBlock().getType().equals(Material.AIR) && !at24.getBlock().getType().equals(Material.BARRIER))
                            || (!at26.getBlock().getType().equals(Material.AIR) && !at24.getBlock().getType().equals(Material.BARRIER))) {

                    } else {
                        Vector vToHub = GeneralConfy.getSpawnHubLocation().toVector().subtract(player.getLocation().toVector()).normalize();
                        player.setVelocity(vToHub.multiply(0.4));
                    }
                }

                if (player.getLocation().getBlockY()+3 >= 45) {
                    player.setVelocity(player.getVelocity().setY(-0.5));
                }

            }

            if (!player.isFlying()
                    && (Main.notBoat != null && !Main.notBoat.contains(player))
                    && player.getLocation().getY() <= 27) {

                if (at24.getBlock().getType().equals(Material.BARRIER)
                        && at25.getBlock().getType().equals(Material.STONE_SLAB2)
                        && (at26.getBlock().getType().equals(Material.WATER) || at26.getBlock().getType().equals(Material.STATIONARY_WATER))) {
                    if (!player.isInsideVehicle()) {
                        Location boatLoc = player.getLocation().clone();
                        boatLoc.setY(27);

                        Boat boat = (Boat) player.getWorld().spawnEntity(boatLoc, EntityType.BOAT);

                        ArrayList<TreeSpecies> list = new ArrayList<>();
                        list.addAll(Arrays.asList(TreeSpecies.values()));
                        Collections.shuffle(list);
                        boat.setWoodType(list.get(0));

                        boat.setInvulnerable(true);

                        boat.setPassenger(player);
                        Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                            @Override
                            public void run() {
                                boatLoc.setYaw(player.getLocation().getYaw());
                                boat.teleport(boatLoc);
                            }
                        }, 2);
                    }
                }
            }

            ArrayList<Entity> nearbyEntities = new ArrayList<>();
            nearbyEntities.addAll(player.getNearbyEntities(0.1, 0.1, 0.1));

            boolean isAnEnderman = false;

            int bucle1 = 0;

            Entity enderman = null;

            while (bucle1 < nearbyEntities.size()) {
                if (nearbyEntities.get(bucle1).hasMetadata("name")) {
                    enderman = nearbyEntities.get(bucle1);
                    isAnEnderman = true;
                    bucle1 = nearbyEntities.size() + 5;
                }
                bucle1++;
            }

            if (isAnEnderman) {
                //player.setVelocity(player.getVelocity().setY(1));


                Vector unitVector = player.getLocation().toVector().subtract(enderman.getLocation().toVector()).normalize();
                player.setVelocity(unitVector.multiply(0.4));
                //enderman.setVelocity(enderman.getVelocity().zero());

                player.playSound(player.getLocation(), Sound.ENTITY_ENDERMEN_TELEPORT, 1, 1);

            }





            if (player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().equals(Material.LOG)
                    && player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.SPONGE)
                    && !player.getOpenInventory().equals(Main.jumpsInventory)) {
                player.openInventory(Main.jumpsInventory);
            }


            Location locAt15 = player.getLocation().clone();
            locAt15.setY(15);

            if (locAt15.getBlock().getType().equals(Material.BARRIER)
                    && player.getLocation().getBlock().getRelative(BlockFace.DOWN).getRelative(BlockFace.DOWN).getType().equals(Material.BARRIER)
                    && player.isOnGround()
                    && player.getLocation().getBlock().getType().equals(Material.GOLD_PLATE)
                    && player.getLocation().getBlock().getRelative(BlockFace.DOWN).getType().isSolid()
                    && !player.isFlying()
                    && player.getGameMode().equals(GameMode.ADVENTURE)
                    && player.getInventory().getItem(1) != null) {
                player.teleport(GeneralConfy.getSpawnHubLocation());
                for (Player pl : player.getWorld().getPlayers()) {
                    pl.sendMessage(Main.masterpikPrefix+(Main.winJumps.replaceAll("%p", player.getName()).replaceAll("%j", player.getInventory().getItem(1).getItemMeta().getDisplayName())));
                }
                player.getInventory().remove(player.getInventory().getItem(1));
                //player.sendMessage("Bravo ! Tu as gagné le "+player.getInventory().getItem(1).getItemMeta().getDisplayName());
                int bucle = 0;
                while (bucle <= 5) {

                    BukkitTask time = Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                        @Override
                        public void run() {
                            Fireworks.explodeFirework(
                                    player.getLocation(),
                                    FireworkEffect.Type.BALL_LARGE,
                                    new ArrayList() {{
                                        add(Color.PURPLE);
                                        add(Color.LIME);
                                        add(Color.YELLOW);
                                        add(Color.AQUA);
                                        add(Color.ORANGE);
                                    }},
                                    new ArrayList() {{
                                        add(Color.MAROON);
                                        add(Color.BLUE);
                                        add(Color.FUCHSIA);
                                        add(Color.ORANGE);
                                    }},
                                    true,
                                    false);
                        }
                    }, bucle*20);

                    bucle++;
                }
            }


            ArrayList<Entity> all = (ArrayList<Entity>) GeneralConfy.getHubWorld().getEntities();

            if (!all.isEmpty()) {

                int bucle = 0;

                while (bucle < all.size()) {

                    if (all.get(bucle).hasMetadata("name")) {
                        if (true/*(all.get(bucle).getMetadata("hub").get(0).asBoolean())*/) {


                            Entity entity = all.get(bucle);
                            /*Location A = entity.getLocation();
                            double XA = A.getX();
                            double YA = A.getY();
                            YA = YA + 2;
                            double ZA = A.getZ();

                            Location B = player.getLocation();
                            double XB = B.getX();
                            double YB = B.getY();
                            YB = YB + 1;
                            double ZB = B.getZ();


                            double alpha = Math.toDegrees(Math.atan((ZB - ZA) / (XB - XA)));


                            float beta = A.getYaw();

                            if (ZB > ZA && XB < XA) {
                                beta = (float) (90 + alpha);
                            } else if (ZB < ZA && XB < XA) {
                                beta = (float) (90 + alpha);
                            } else if (ZB < ZA && XB > XA) {
                                beta = (float) (-90 + alpha);
                            } else if (ZB > ZA && XB > XA) {
                                beta = (float) (-90 + alpha);
                            } else if (ZB == ZA && XB == XA) {
                                beta = beta;
                            } else if (ZB == ZA) {
                                if (XB > XA) {
                                    beta = -90;
                                } else if (XB < XA) {
                                    beta = 90;
                                }
                            } else if (XB == XA) {
                                if (ZB > ZA) {
                                    beta = 0;
                                } else if (ZB < ZA) {
                                    beta = 180;
                                }
                            }*/

                            //player.sendMessage("alpha = "+alpha+" | beta = "+beta);



                            /*double alpha2 = Math.toDegrees(Math.atan(((YB-YA)/(Math.sqrt(((XB-XA)*(XB-XA))+((ZB-ZA)*(ZB-ZA)))))));

                            float beta2 = A.getPitch();

                            if (YB == YA) {
                                beta2 = 0;
                            } else if (YB >= YA) {
                                beta2 = (float) -alpha2;
                            } else if (YB <= YA) {
                                beta2 = (float) -alpha2;
                            }

                            if (beta2 > 180) {
                                beta2 = 180;
                            } else if (beta2 < -180) {
                                beta2 = -180;
                            }*/

                            //player.sendMessage("you moove beta2 is "+beta2);

                            //((CraftEntity)entity).getHandle().setPositionRotation(A.getX(), A.getY(), A.getZ(), beta, A.getPitch());

                            //UtilEntity.setAiEnabled(entity, true);

                            /*Location ne = A.clone();
                            ne.setYaw(beta);
                            ne.setPitch(beta2);
                            entity.teleport(ne);*/

                            float beta = Utils.getBeta(entity, player);
                            float beta2 = Utils.getBeta2(entity, player);

                            WrapperPlayServerEntityHeadRotation packet1 = new WrapperPlayServerEntityHeadRotation();

                            packet1.setEntityId(entity.getEntityId());
                            packet1.setHeadYaw(beta);

                            packet1.sendPacket(player);


                            WrapperPlayServerEntityLook packet = new WrapperPlayServerEntityLook();

                            packet.setEntityID(entity.getEntityId());
                            packet.setYaw(beta);
                            packet.setPitch(beta2);
                            packet.setOnGround(true);

                            packet.sendPacket(player);

                            //UtilEntity.setAiEnabled(entity, false);


                        }
                    }

                    bucle++;
                }
            }



        }
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void AsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        event.setCancelled(true);

        Chat.sendAMessage(event.getPlayer(), event.getMessage());

        //event.setMessage(Mplayer.getStatu().getColor()+" "+Mplayer.getPlayer().getName()+" a le grade "+Mplayer.getStatu().getName());


    }
    
    @EventHandler(priority = EventPriority.HIGHEST)
    public void PlayerSwapHandItemsEvent(PlayerSwapHandItemsEvent event) {
        if (event.getPlayer().getWorld().equals(GeneralConfy.getHubWorld())) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void PlayerGameModeChangeEvent(PlayerGameModeChangeEvent event) {
        Player player = event.getPlayer();

        MasterpikPlayer Mplayer = Players.players.get(event.getPlayer().getUniqueId());

        if (Mplayer != null
                && Mplayer.getStatu().isHubElytra()) {
            Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                @Override
                public void run() {
                    player.setAllowFlight(true);
                }
            }, 1L);
        }
    }

    @EventHandler
    public void VehicleDestroyEvent(VehicleDestroyEvent event) {

        if (event.getVehicle().getWorld().equals(GeneralConfy.getHubWorld())
                || (event.getVehicle().getWorld().getName().equals("hub2"))) {
            if (event.getVehicle().getType().equals(EntityType.BOAT)) {
                event.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void VehicleExitEvent(VehicleExitEvent event) {
        if (event.getVehicle().getWorld().equals(GeneralConfy.getHubWorld())
                || (event.getVehicle().getWorld().getName().equals("hub2"))) {
            if (event.getVehicle().getType().equals(EntityType.BOAT)
                /*&& event.getVehicle().isEmpty()*/
                    && event.getExited().getType().equals(EntityType.PLAYER)) {

                Player player = (Player) event.getExited();

                event.getVehicle().remove();

                Main.notBoat.add(player);
                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        if (Main.notBoat != null
                                && Main.notBoat.contains(player)) {
                            Main.notBoat.remove(player);
                        }
                    }
                }, 20 * 1);

                Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable() {
                    @Override
                    public void run() {
                        Vector vToHub = GeneralConfy.getSpawnHubLocation().toVector().subtract(event.getExited().getLocation().toVector()).normalize();
                        vToHub.setY(0.8);
                        event.getExited().setVelocity(vToHub.multiply(0.6));
                    }
                }, 1);


            }
        }
    }

    @EventHandler
    public void EntityToggleGlideEvent(EntityToggleGlideEvent event) {
        if (event.getEntityType().equals(EntityType.PLAYER)) {
            if (event.getEntity().getWorld().equals(GeneralConfy.getHubWorld())
                    || event.getEntity().getWorld().getName().equals("hub2")) {
                Player player = (Player) event.getEntity();

                if (!event.isGliding()) {
                    player.getInventory().setChestplate(new ItemStack(Material.AIR));
                }

            }
        }
    }

    @EventHandler
    public void LeavesDecayEvent(LeavesDecayEvent event) {
        event.setCancelled(true);
    }

}
